package id.sch.smktelkom_mlg.lifecycle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class Main2Activity extends AppCompatActivity {


    private static final String TAG = Main2Activity.class.getSimpleName();


    private static final String ON_CREATE = "onCreate";

    private static final String ON_START = "onStart";

    private static final String ON_RESUME = "onResume";

    private static final String ON_PAUSE = "onPause";

    private static final String ON_STOP = "onStop";

    private static final String ON_RESTART = "onRestart";

    private static final String ON_DESTROY = "onDestroy";

    private static final String ON_SAVE_INSTANCE_STATE = "onSaveInstanceState";


    private TextView mLifecycleDisplay;


    /**
     * Called when the activity is first created. This is where you should do all of your normal
     * <p>
     * static set up: create views, bind data to lists, etc.
     * <p>
     * <p>
     * <p>
     * Always followed by onStart().
     *
     * @param savedInstanceState The Activity's previously frozen state, if there was one.
     */

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);


        mLifecycleDisplay = findViewById(R.id.tv_lifecycle_events_display);


        // TODO (6) If savedInstanceState is not null and contains LIFECYCLE_CALLBACKS_TEXT_KEY, set that text on our TextView


        logAndAppend(ON_CREATE);

    }


    /**
     * Called when the activity is becoming visible to the user.
     * <p>
     * <p>
     * <p>
     * Followed by onResume() if the activity comes to the foreground, or onStop() if it becomes
     * <p>
     * hidden.
     */

    @Override

    protected void onStart() {

        super.onStart();


        logAndAppend(ON_START);

    }


    /**
     * Called when the activity will start interacting with the user. At this point your activity
     * <p>
     * is at the top of the activity stack, with user input going to it.
     * <p>
     * <p>
     * <p>
     * Always followed by onPause().
     */

    @Override

    protected void onResume() {

        super.onResume();


        logAndAppend(ON_RESUME);

    }


    /**
     * Called when the system is about to start resuming a previous activity. This is typically
     * <p>
     * used to commit unsaved changes to persistent data, stop animations and other things that may
     * <p>
     * be consuming CPU, etc. Implementations of this method must be very quick because the next
     * <p>
     * activity will not be resumed until this method returns.
     * <p>
     * <p>
     * <p>
     * Followed by either onResume() if the activity returns back to the front, or onStop() if it
     * <p>
     * becomes invisible to the user.
     */

    @Override

    protected void onPause() {

        super.onPause();


        logAndAppend(ON_PAUSE);

    }


    /**
     * Called when the activity is no longer visible to the user, because another activity has been
     * <p>
     * resumed and is covering this one. This may happen either because a new activity is being
     * <p>
     * started, an existing one is being brought in front of this one, or this one is being
     * <p>
     * destroyed.
     * <p>
     * <p>
     * <p>
     * Followed by either onRestart() if this activity is coming back to interact with the user, or
     * <p>
     * onDestroy() if this activity is going away.
     */

    @Override

    protected void onStop() {

        super.onStop();


        logAndAppend(ON_STOP);

    }


    /**
     * Called after your activity has been stopped, prior to it being started again.
     * <p>
     * <p>
     * <p>
     * Always followed by onStart()
     */

    @Override

    protected void onRestart() {

        super.onRestart();


        logAndAppend(ON_RESTART);

    }


    /**
     * The final call you receive before your activity is destroyed. This can happen either because
     * <p>
     * the activity is finishing (someone called finish() on it, or because the system is
     * <p>
     * temporarily destroying this instance of the activity to save space. You can distinguish
     * <p>
     * between these two scenarios with the isFinishing() method.
     */

    @Override

    protected void onDestroy() {

        super.onDestroy();


        logAndAppend(ON_DESTROY);

    }


    // TODO (2) Override onSaveInstanceState

    // Do steps 3 - 5 within onSaveInstanceState

    // TODO (3) Call super.onSaveInstanceState

    // TODO (4) Call logAndAppend with the ON_SAVE_INSTANCE_STATE String

    // TODO (5) Put the text from the TextView in the outState bundle


    /**
     * Logs to the console and appends the lifecycle method name to the TextView so that you can
     * <p>
     * view the series of method callbacks that are called both from the app and from within
     * <p>
     * Android Studio's Logcat.
     *
     * @param lifecycleEvent The name of the event to be logged.
     */

    private void logAndAppend(String lifecycleEvent) {

        Log.d(TAG, "Lifecycle Event: " + lifecycleEvent);


        mLifecycleDisplay.append(lifecycleEvent + "\n");

    }


    /**
     * This method resets the contents of the TextView to its default text of "Lifecycle callbacks"
     *
     * @param view The View that was clicked. In this case, it is the Button from our layout.
     */

    public void resetLifecycleDisplay(View view) {

        mLifecycleDisplay.setText("Lifecycle callbacks:\n");

    }

}